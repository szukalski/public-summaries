# The Schmuck in My Office: Summary

* 10 basic disruptive personality types.  
* Use concise and direct communication

## Strategies for Handling Disruptive Personalities

* A disruptive personality may seem to control 90-100% of a relationship &rarr; All you need to do is mind your 50% and use the strategies to keep the relationship as healthy as possible
* People want connection:
    * Talk directly to people, live and in person
    * Look into their eyes as they speak
    * Listen to their tone, get a sense of their physical energy
    * Hear what they are saying without jumping to conclusions &rarr; Those conclusions may be more about what you're bringing to the table than what they are

## The Narcissus

### Traits

* Only cares about his own success and has no concern for the well-being of others
* Can be very charismatic
* Very sensitive to criticism

### Types

* Look at me
    * Compliment fishing
    * Controls conversation
    * Avoids competition
    * *My way or the highway*
* Woe is me
    * Panders for reassurance
* Impossible
    * Simply cannot think beyond himself and very little empathy

### Dealing Effectively

* Keep them from feeling threatened
    * Flatter with compliments and praise
    * Sandwich a request between compliments
* Respond quickly to their requests to show their importance
* Encourage them to see the perspective of others
* **Remember the potential for them to sacrifice you for their own gain**

## The Venus Flytrap

### Traits

* Unstable sense of identity and unable to control their emotions
* Impulsive and potentially dangerous behaviour
* Empty feeling inside despite intense emotions

### Types

* Edgy
    * Always in battle, seeking an enemy
    * May seem competent, then suddenly derail
    * Needs to see who is there for them, and who truly needs them
* Downer
    * Always *glass half empty*
    * Deeply sensitive to perceived rejections
    * Makes others feel responsible for keeping their misery at bay

### Dealing Effectively

* Continuously define boundaries and clearly reinforce them
* Recognise and redirect emotions, rather than restrict them without rationale
    * Do not tell them that they cannot feel a certain way
* Practice *chain analysis* to determine the chain of events which led to the behaviour, the consequences, and how to avoid it in the future
* Redirect emotional outbursts to healthy alternative activities
* Avoid getting pulled into the drama **at all costs**

## The Swindler

### Traits

* Commits acts of antisocial behaviour without any concern for others
* May have superficial appeal but are insincere in their words and actions
* Has a general pattern of deceit and manipulation

### Types

* Fast Eddie
    * Just wants little things for sport, superficial convenience, or shortcuts
    * Tries to get the most for the least
* The Sleaze
    * Wholly committed manipulator
    * Does not try, or is unable, to hide his slimy ways
    * Will always screw you over sooner or later
* Big Boss Jr.
    * Extended version of *Fast Eddie* and *The Sleaze* but more sophisticated
* Big Boss Sr.
    * True criminal
    * May lead double life
* The Serial Killer
    * Psychopath who embodies true evil

### Dealing Effectively

* Avoid hiring at all costs
    * Talk to references, conduct background checks, look for warning signs on resumes and interviews
* Prevent swindlers rising in the ranks by focusing on contribution to the company over personal successes
* Develop code of conduct and communicate it effectively
* Enforce violations consistently
* Remove swindlers once detected
* Ensure clear documentation and evidence of transgressions

## The Bean Counter

### Traits

* Overwhelming obsession with details
* Need to control the uncertainty of life
    * Try to artifically impose organisation through rules, laws, and protocols
* Clings to perfection which gets in the way of accomplishment

### Types

* Hostile Bean Counter
    * Hard on others
    * Unable to delegate tasks
    * Control freak
    * Appears logical and intellectual but is long-winded, hypercritical, and dogmatic
* Hermit Bean Counter
    * Hard on themselves
    * Self-doubt
    * Difficulty asserting themselves, afraid of social disapproval and rejection

### Dealing Effectively

* Avoid direct challenges to their detail orientated nature
* Acquiesce to their logic helps achieve your results
* Never promise more than you can deliver
* During evaluation, normalise mistakes and stress that perfection can be the enemy of good
* Express appreciation of their dedication

## The Distracted

### Traits

* Attention problems
* Struggle with organisation
* Difficulty with sitting still, being quiet, and waiting their turn

### Dealing Effectively

* Assign small, achievable projects with step-by-step tasks
* Increase engagement with creative components
* Preferred management style is clear, patient, and predictable, without micro-management
* Encourage to not over-commit, and focus on finishing one task before starting the next
* Training in time management, and planning

## Mr Hyde

### Traits

* Drug use without control
* Predisposition to negative emotions
* Lack of inhibition
* Antagonistic
* Change in behaviour from the baseline

### Dealing Effectively

* Confront assertively, but empathetically
* Provide example of increasingly problematic behaviour over time
* Reinforce that you are there for them, even though they are responsible for change
* Document conversations and plans
* Acknowledge that setbacks will occur, and are not *failures*
* Be firm with limits and consequences

## The Lost (Dementia)

### Traits

* Broad difficulty with using one's previous intelligence

### Types

* Full-of-Excuses
    * Struggles with mistakes by fighting back and creating stories to cover noted deficits
    * May bring up things from the past, use broad insults, or question people's authority to judge their work
* Lost Under the Radar
    * Feel embarrassed about loss in memory
    * Struggles to work out what is happening

### Dealing Effectively

* Have supportive conversation to help them recognise the difficulties they are having and how these are affecting their life and work performance
* Safety is a primary concern
* Use memory prompts and technology aids
* Assign structured tasks
* Use clear and simple language spoken slowly and loudly

## The Robotic (Autism)

### Traits

* Inflexibility and difficulty communicating with and understanding others
* Impaired social interactions and rigidity
* Restricted set of interests and routines

### Types

* Circuit Overloader
    * Full of opinions and no ability to filter what should be said to whom
    * Personal opinions are facts
    * Offends constantly with no sensitivity in doing so
* Failed Igniter
    * Withdrawn and socially avoidant
    * Lacks normal social skills

### Dealing Effectively

* Clear and concrete communication
    * Written may be preferred to verbal
* Minimise distractions
* Rigid, predictable schedules and explicitely defined tasks
* Incorporation of tasks related to special interests
* Define rules for appropriate behaviour

## The Eccentric

### Traits

* Difficulty relating to people because of unique ways of seeing the world
* Often tied to magic or the paranormal
* Often appear detached, isolated, and aloof

### Types

* Sorceror
    * Not afraid to share their (big) ideas
    * Everything comes back to their overvalued ideas
* Sore Thumb
    * Stands out, but not sure why
    * May have weird ideas but not likely to make them known

### Dealing Effectively

* Where possible, employ in an environment which caters to their special interests
* Reinforce that they are held to the same standard of work quality as others
* Be gently direct in addressing odd or inappropriate behaviour

## The Suspicious

### Traits

* Always on the lookout for harm, exploitation, and deception
* Relationships tend to be judged in terms of loyalty and trust
* Hard to handle ambiguity
* Potentially dangerous

### Dealing Effectively

* Be clear and direct with communication and provide transparent rationale for decisions
* Offer a choice in alternatives where possible to help them feel in more control
* Use simple statements without being confrontational
* Safety of the workplace is paramount

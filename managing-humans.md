# Managing Humans

Managing Humans: Biting and Humorous Tales of a Software Engineering Manager  
Michael Lopp  
ISBN-10: 1484221575  
ISBN-13: 978-1484221570  

## Listening

* Open with innocuous preamble.
    * Define the quiet, safe place.
* Maintain eye contact and don’t look at the clock.
* Be a curious fool.
    * Ask stupid questions until you understand the situation.
* Validate ambiguity, maps their words to yours, and build gentle segues.
    * Ensure you understand the message they are trying to convey.
    * Restatement of what is said shows you are listening and understanding.
    * “Ok, next thought?” .. “And then what happened?”
* Pause. Shut up.
    * Let them speak.

## Three Leaders

### The Lead

* Beginning of career of leading.
* Responsible for a single team.
* Focus on the team.
* Tactical, and showing signs of strategy.

### The Lead of Leads

* Responsible for multiple teams.
* Focus across the company.
* Equal parts tactics and strategy.

### The Director

* Focus is outward.
* Interface between company and world.
* Mainly strategy.

## When The Sky Falls

* The Elusive Step 0
    * What, precisely, are you trying to do?
* Step 1: The Situation in the War Room
    * Understand everything you need to know about the current state of the disaster.
    * Information acquisition, not action.
    * What has actually occurred.
    * List of additional research, work, and potential next actions is developed, iterated, and prioritised (but not actioned yet)
    * Find the glimpse of What we need to do
* Step 2: The Bet Your Car Perspective
    * Vet your model with at least 3 qualified others.
    * Put the current version of the action plan on the whiteboard and ask the responsible implementers if it makes sense.
* Step 3: Constant and Consistent Sky-Propping Pressure
    * Keep the status updates coming.
    * Stop the grapevine.

## Incrementalists and Completionists

* Incrementalists:
    * Realists
    * Continuously making progress
    * Tactical
    * Need vision
        * Want to implement solution to fix the right now, but don’t see the longer term impact
    * Get them to see the big picture
* Completionists:
    * Dreamers
    * Want to solve a problem with the best possible solution and cover it for years to come
    * Strategic
    * Need action
        * Can see the longer term impact, but can’t communicate the requirements
    * Get them to communicate the solution effectively

## Mechanics and Organics

### Mechanics

* Move forward medthodically
* Gather information in a structured manner and store it efficiently
* Quietly observe, stay on message, and are comfortably predictable
* Need to push information to them in a structured manner
    * Status template

### Organics

* All over the place
* Loud and can tell a joke
* Ask seemingly meaningless questions
* Good networking
* Trust that they have a plan

## The Vision Heirarchy

### Inwards

* Team Lead
* Responsible for a small team focusing on a single technology
* Focus is on their team or technology
* Stays near code

### Holistics

* Middle layer of management
* Manager of managers
* Focus is across the organisation
* Need to know what is going on everywhere
* Have star Inwards to deliver

### Outwards

* Senior Manager
* Focused on the outside world
* The don’t run the company on a daily basis (holistics do) but they are accountable for it.

## A Glimpse and a Hook

Building a resume and selling yourself

### First Pass

* Your name
    * Are you recognised? Is there a web presence?
* Company names
* Job description and history
* Other interests
    * What makes you different?

### Second Pass

* In-depth job history
    * Do responsibilities match the title?
    * Do the jobs build on each other?
    * Is it clear where you are headed?
* School

### Differentiate, Don’t Annoy

* Design resume to downgrade
    * Needs to withstand reformatting abuse
* Never in clude cover letter
* Embrace buzz-word compliance
    * Pass the recruiter test
* Differentiate, don’t annoy
    * Deviate from the standard template to attract the reader
* Sound like a human
    * Don’t use resume mumbo jumbo
    * Is it clear what was done?
* Include seemingly irrelevant experience
    * What did you learn?

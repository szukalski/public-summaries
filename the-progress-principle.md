# The Progress Principle

The Progress Principle  
Teresa Amabile, Steven Kramer  

## The Progress Principle: Summary

**Progress Principle**: The single most powerful positive effect which influences progress is **progress in meaningful work**

### The Three Components of Inner Work Life

1. Perceptions/Thoughts
    * The organisation
    * Managers, team, self
    * The work
    * Sense of accomplishment
2. Emotions/Feelings
    * Positive emotions
    * Negative emotions
    * Overall mood
3. Motivation/Drive
    * What to do
    * Whether to do it
    * How to do it
    * When to do it

### The Key Three Influences on Inner Work Life

1. The Progress Principle
    * Events signifying progress, including:
        * Small wins
        * Breakthroughs
        * Forward Movement
        * Goal completion
2. The Catalyst Factor
    * Events supporting the work, including:
        * Setting clear goals
        * Allowing autonomy
        * Providing resources
        * Providing sufficient time
        * Helping with the work
        * Learning from problems and successes
        * Allowing ideas to flow
3. The Nourishment Factor
    * Events supporting the person, including:
        * Respect
        * Encouragement
        * Emotional support

## The Power of Negative Effects

To foster great inner work life, focus first on eliminating the obstacles that cause setbacks.  
One setback has more power than one progress incident:
    * Effect of setbacks on emotions is stronger than the effects of progress
    * Small losses can overwhelm small wins
    * Negative team leader behaviours have more effect than positive

## What is Meaningful Work?

To be meaningful, your work doesn't have to have profound importance to society.  
What matters is whether you *perceive* your work as contributing value to something or someone.  

Managers should:
    * Ensure that employees know how their work is contributing
    * Avoid actions that negate the value of work

### Four Ways to Negate Meaning

1. Having work or ideas dismissed
2. Losing a sense of ownership
3. Having doubt that work will be realised
4. Doing work which you are over-qualified for

### The Progress Loop

#### Positive Form

Progress &larr; &rarr; Positive inner work life

#### Negative Form

Setbacks &larr; &rarr; Negative inner work life

## The Catalyst Factor

Catalysts and inhibitors can have an immediate impact on inner work life, before they can impact the work itself.  

Catalyst
    * Directly facilitates the timely, creative, high-quality completion of work
Inhibitor
    * Absence or negative form of catalyst

### The Seven Major Catalysts

1. Setting Clear Goals
2. Allowing Autonomy
3. Providing Resources
4. Giving Enough Time, But Not Too Much
5. Help With the Work
6. Learning from Problems and Successes
7. Allowing Ideas to Flow

### Climate Forces Shaping Catalysts

1. Consideration for people and their ideas
2. Coordination
3. Communication

## The Role of Team Leaders in the Catalyst Factor

Team leaders and immediate coworkers have more influence on inner work life than top management and the organisation.

|As a team leader, *do*..|As a team leader, *don't*..|
|-|-|
|Gather information that could be helpful to the team|Fail to disseminate relevant information to the team |
|Involve the team in making important decisions|Micromanage|
|Develop external networks who could inform and support the team|Fail to set an example with your own behaviour|
|Sell and fight for the team|Avoid problems|
|Set clear assignments and goals|Fail to show strategy and goals|

## The Nourishment Factor

Leading by serving does not abdicate responsibility. But it does require a wholly different mind-set toward management - focusing not on traditional control of subordinates, but on contribution to real work progress by team members.  

### The Four Major Nourishers

1. Respect
    * Recognition
    * Honesty
    * Civility
2. Encouragement
    * Manager's enthusiasm can help increase employee motivation
    * Manager's confidence in people's ability to do work increases their sense of self-efficacy
3. Emotional Support
    * Empathy is better than acknowledgement
4. Affiliation
    * When people enjoy each other, there are fewer negative interpersonal conflicts

### The Four Toxins

1. Disrespect
2. Discouragement
3. Emotional Neglect
4. Antagonism

### The Role of Team Leaders in the Nourishment Factor

| As a team leader, *do*..|As a team leader, *don't*..|
|-|-|
|Show respect to people and their work|Act dismissive, discourteous, or patronising|
|Recognise and reward accomplishments|Display apathy to people or their work|
|Provide emotional support when needed|Change people's roles haphazardly|
|Create opportunities for development of friendship and camaraderie in the team||

## Daily Progress Checklist

* Use the checklist to review the day and plan the next day
* Aciton plan for next day is most important part of daily review

## Checklist

|Progress|Setbacks|
|---|---|
|1-2 events indicating small win or possible breakthrough|1-2 events indicating small setback or possible crisis|

|Catalysts|Inhibitors|
|---|---|
|Team is clear on short and long term goals|Confusion on goals|
|Team members have autonomy to solve problems and take ownership|Team members constrained|
|Resources needed are available to move forward|Resources are not available|
|Sufficient time to focus on meaningful work|Lack sufficient time|
|Did I provide help when needed or requested?|Did I fail to give help?|
|Did I discuss lessons from today's successes and problems?|Did I punish failure or neglect to find lessons and/or opportunities in problems and successes?|
|Did I help ideas flow freely?|Did I cut off the presentation or debate of ideas prematurely?|

|Nourishers|Toxins|
|---|---|
|Did I show *Respect* by recognition of contributions, ideas, and treating as trusted professionals?|Did I *Disrespect* anyone?|
|Did I *Encourage* team members for facing difficult challenges?|Did I *Discourage* in any way?|
|Did I *Support* team members who had a personal or professional problem?|Did I *Neglect* team members who needed support?|
|Is there *Affiliation* in the team?|Is there *Antagonism* in the team?|

|Inner Work Life|
|---|
|Did I see any indications of the quality of my team members' inner work lives today?|
|   * Perceptions|
|   * Emotions|
|   * Motivation|
|What specific events might have affected inner work life today?|

|Action Plan||
|---|---|
|What can I do tomorrow to strengthen the catalysts and nourishers identified and provide ones which are lacking?|What can I do tomorrow to start eliminating the inhibitors and toxins identified?|

## Daily Journaling

### Guidelines for Daily Journaling

Answer the following questions:
    * What event stands out from the workday and how did it affect my inner work life?
    * What progress did I make today and how did it affect my inner work life?
    * What nourishers and catalysts supported me and my work today? How can I sustain them tomorrow?
    * What one thing can I do to make progress on important work tomorrow?
    * What setbacks did I have today, and how did they affect my inner work life? What can I learn from them?
    * What toxins and inhibitors impacted me and my work today? How can I weaken or avoid them?
    * Did I affect my colleagues' inner work lives positively today? How might I do so tomorrow?

## The Progress Principle: Review

Overall, useful information but told in a story-like structure so learning is made more complicated than it needs to be.

Would benefit from chapter summaries of the take-away information.
    * Technical people will find it hard to gather the information in a clear and concise manner

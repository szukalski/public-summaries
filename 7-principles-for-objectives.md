# 7 Principles for Objectives

1. Few but big objectives
2. Quantification
3. Consider required resources
4. Personalise responsibility (individuals, not groups)
5. Appropriate monitoring intervals
6. Objectives in writing
7. Participation wherever possible

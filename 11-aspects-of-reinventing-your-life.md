# 11 Aspects of Reinventing Your Life

1. Reinvention is a Habit
    * A river never stays still
2. The One Takeaway
    * Learn one thing from everyone you meet
3. Meaning
    * Meaning is the building block for motivation
    * Write down the things that give you meaning
4. Contagious
    * Make sure you are infected with the virus that propels you each day to live a better life than the day before
5. Ideas are Currency
    * Start writing ideas
    * Exercise the idea muscle every day
6. What are your Five?
    * What are the five things you need to think about every day?
    * Don't focus on anything else
7. Perseverance
    * Success is about how you overcome a setback and keep going
8. Do the Unexpected
    * What has nobody done that I can now do?
    * Unexpected is the God of Reinvention
9. Combination Sex
    * Take 2-3 things and combine them &rarr; become the best at the intersection
10. Plus, Minus, Equal
    * Everyone needs a Plus, Minus, Equal to learn
        * **Plus**: Someone to mentor you
        * **Minus**: People to teach to
        * **Equal**: People to challenge you
11. The Daily Practice
    * Get 1% better at each aspect of health each day &rarr; 3800% better each year!
        * Physical, emotional, mental, spiritual

# The Power of Moments

## Building Defining Moments

Elevation
    * Experiences that rise above the routine
        1. Boost the sensory appeal
        2. Raise the stakes
        3. Break the script
Pride
    * Commemorates people's achievements
        1. Recognise others
        2. Multiply meaningful milestones
            * Reframe a long journey to feature multiple "finish lines"
        3. Practice courage by preloading responses in advance
            * Ready when the right moment comes

Insight
    * Deliver realisations and transformations
    * To produce moments of insight in others:
        1. Reveal a clear insight
        2. Compressed in time
        3. Discovered by the audience
    * To produce moments of self-insight we must stretch and place ourselves in situations which involve the risk of failure

Connection
    * Bond us together
    * Groups unite when they struggle together toward a meaningful goal
        * Often begin their work with a synchronised moment
    * Responsiveness deepens our ties
        * A responsive interaction can bring people together very quickly

## Create Defining Moments

1. Write a "gratitude letter" to someone who has made a positive difference in your life
2. "Break the script" in some part of your life that has grown too routine
3. Push beyond small talk with someone in your life

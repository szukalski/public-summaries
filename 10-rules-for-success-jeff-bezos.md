# 10 Rules for Success: Jeff Bezos

1. Regret Minimisation
    * Seek to minimise the number of regrets you will have in life.
2. Follow Your Heart Not Your Head
3. Invest More in the Product than Marketing
4. Pick a Good Name
5. Stand For Something
6. Focus on the Customer
7. Focus on Your Passion
8. Build a Culture
    * Be clear on what you do.
9. Premium Products at Non-Premium Prices
10. Take A Risk

# Speak like a Leader

Speak like a Leader
Simon Lancaster
https://youtu.be/bGBamfWasNQ

## Speak Like a Leader: Summary

1. Three Breathless Sentences
    * 3 is the magic number.
    * Put people on edge.
    * Makes it sound more compelling and credible.
2. Three Repetitive Sentences
    * Same introduction each time (I love cheese, I love pasta, I love wine!)
    * Conveys passion.
3. Three Balancing Statements
    * If the sentence sounds balanced then we assume the thinking is balanced.
    * Create balance with opposites (Look to the future, not to the past. What we can do, not what we can’t).
4. Metaphor
    * Used to draw people towards things (beautiful images) or push them away (disgusting images).
    * Enormous impact.
5. Exaggeration
    * When we are emotional, we exaggerate. Thus exaggeration conveys emotion and passion.
6. Rhyme
    * People are more likely to believe something is true if it rhymes.

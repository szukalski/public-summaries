# 18 Essential Lessons from a Self Made Millionaire

1. Don’t piss away your life on work.
2. Don’t piss away your money on lifestyle.
3. Happiness and money are not correlated. You can be happy and broke.
4. Being broke sucks because it limits personal freedom.
5. Happiness is a thing – not a thing to be acquired. A fulfilling life is filled with experiences – not stuff.
6. The essential pre-requisite to financial freedom is commitment.
7. Once committed to financial freedom, arrange your life and support structure towards achieving that goal with the least effort and maximum persistence.
8. Commitment to financial freedom is worthless without a plan.
9. A plan for financial freedom is next-to-worthless unless it is based on proven principles. It should also focus on your values, skills, and resources.
10. Leverage is an essential principle of wealth building.
11. Investing is an essential skill for financial freedom. Regardless of where the wealth originated, you must become a master investor for where it will end up.
12. Always maintain an income stream greater than your expenses. Never live off principal.
13. Proper investment strategy is a discipline where you only put capital at risk when there is a proven model with a positive mathematical expectation. Everything else is a gamble and casinos always win.
14. The math of compound financial growth over very long periods is complicated. The equations are simple but the real life application is more complex.
15. Financial freedom occurs when passive income exceeds expenses.
16. Retirement is over-rated. Meaningful work or a bigger purpose builds a deeply satisfying life which requires no retirement.
17. Continuum of Business and Investing. The amount of wealth generation possible is related to the amount of time spent on the method. Business takes all your time, passive investing takes very little time, active investment has an element of business, and real estate is half business-half investment.
18. Don’t get involved into a business unless the things you do each day are consistent with your values and provide a fun, creative outlet. Life is too short.

# Multipliers

Multipliers: How the Best Leaders Make Everyone Smarter  
Liz Wiseman  
ISBN-10: 0062663070  
ISBN-13: 978-0062663078  

## Multipliers: Summary

By extracting people's full capacity, Multipliers get twice the capability from people than do Diminishers

People are smart and the job of the leader is to draw out the intelligence from others

### Multipliers versus Diminishers

| Multipliers | Diminishers |
|-------------|-------------|
| Genius makers | Absorbed in their own intelligence |
| Bring out the intelligence in others | Stifle others |
| Build collective, viral intelligence in organisations | Deplete the organisation of crucial intelligence and capability |

### Five Disciplines of the Multipliers

1. Talent Magnet
    * Attract and optimise talent
2. Liberator
    * Require people's best thinking
3. Challenger
    * Extend challenges
4. Debate Maker
    * Debate decisions
5. Investor
    * Instill accountability

| Multiplier | Diminisher |
|-|-|
|Talent Magnet|Empire Builder|
|Liberator|Tyrant|
|Challenger|Know-It-All|
|Debate Maker|Decision Maker|
|Investor|Micro Manager|

## The Multiplier Effect

1. Most people are underutilised
2. All capability can be leveraged with the right kind of leadership
3. Therefore, intelligence and capability can be multiplied without bigger investment

### Mind of the Multiplier

* Mind of the Diminisher
    * Intelligent people are a rare breed
    * Others will never figure things out without me
    * Intelligence is static
* Mind of the Multiplier
    * In what way is this person smart?
    * Intelligence is continuously developing
    * People are smart and will figure it out

### Surprising Findings

* A Hard Edge
    * Not *feel-good* managers
    * High expectations
* A Great Sense of Humour
    * Don't take themselves too seriously
    * Use humour to create comfort and spark a natural energy and intelligence in others
* The Accidental Diminisher
    * Few diminishers realise the restrictive impact they have on others

## The Talent Magnet

| Empire Builders | Talent Magnets |
|-|-|
| Bring in great talent but underutilise it | Have great people flock to them to be utilised |
| Hoard resources for their own gain | Develop people for the next stage |

### Four Practices of the Talent Magnet

1. Look for Talent Everywhere
2. Find People'S Native Genius
3. Utilise People to their Fullest
    * Connect to opportunities
    * Shine a spotlight
4. Remove the Blockers
    * Get rid of prima donnas
    * Get out of the way

### Becoming a Talent Magnet

1. Become a genius watcher
    * Identify genius
    * Verify it with feedback
    * USe it in other opportunities
2. Pull some weeds
    * Remove diminishers

## The Liberator

| Tyrants | Liberators |
|-|-|
| Tense environment | Intense environment |
| Suppresses thinking and capabilities | Requires best thinking and capabilities |
| People hold back | People offer ideas |
| Safe thinking | Bold thinking |
| Work cautiously | Best effort |

### Three Practices of the Liberator

1. Create Space
    * Restrain yourself
    * Listen, not talk
    * Be consistent
2. Demand Best Work
    * Defend the standard
3. Generate Rapid Learning Cycles
    * Admit and share mistakes
    * Insist on learning from mistakes

### Becoming a Liberator

1. Play your (poker) chips
    * Limit your contributions in meetings to a set number
2. Label your opinions
    * *Soft* opinions are meant to provoke thought
    * *Hard* opinions are meant to declare guidelines
3. Make your mistakes known
    * Get personal
    * Go public

## The Challenger

| Know-it-alls | Challengers |
|-|-|
| Give directives to show how much they know | Define opportunities to push people beyond what they can do |
| Org limited to what boss knows | Org can focus on challenges |
| Org energy used to deduce what boss wants | Org energy focused on challenge |

### Three Practices of the Challenger

1. Seed the Opportunity
    * Show the need
    * Challenge assumptions
    * Reframe problem
    * Create starting point
2. Lay Down a Challenge
    * Extend a concrete challenge
    * Ask the hard questions
    * Let others fill in the blanks
3. Generate Belief in What is Possible
    * Helicopter down to show the challenge can be met
    * Lay out a path
    * Co-create the plan
    * Orchestrate an early win

### Becoming a Challenger

1. Ask a leading question
2. Take a *bus trip* with everyone to see the need that must be met
3. Take a massive baby step

## The Debate Maker

| Decision Makers | Debate Makers |
|-|-|
| Decide efficiently with a small inner circle | Engage people in debating issues upfront |
| Leave broader organisation in the dark to debate the soundness of the decision instead of executing it | People understand decision and can execute efficiently |

### The Three Practices of the Debate Maker

1. Frame the Issue
    * Define the question
    * Form the team
    * Assemble the data
    * Frame the decision
2. Spark the Debate
    * Create safety for best thinking
    * Demand rigor
3. Drive a Sound Decision
    * Reclarify the decision-making process
    * Make the decision
    * Communicate the decision and rationale

### Becoming a Debate Maker

1. Ask the hard question
    * Discussion leader only asks questions
    * Hold your views
2. Ask for the data
    * Must be evidence to support their theories
    * Make it a norm to come into a debate with data
3. Ask each person
    * Everyone participates
    * Softer voices often have more analytical views

## The Investor

| Micromanagers | Investors |
|-|-|
| Manage every detail | Give other people the investment and ownership to produce results |
| Dependence on the leader | Independence |

### The Three Practices of the Investor

1. Define Ownership
    * Name the lead
    * Give ownership for the end goal
    * Give the role
2. Invest Resources
    * Teach and coach
    * Provide backup
3. Hold People Accountable
    * Give back feedback and guidance
    * Expect complete work
    * Respect natural consequences
    * Make the scoreboard visible

### Becoming an Investor

1. Let them know who is boss
    * They are! 51% stake
2. Let nature take it's course
    * We learn from our own experiences and mistakes
        1. Let failures happen
        2. Talk about what happened
        3. Focus on next time
3. Ask for the F-I-X
    * Solutions, not problems
4. Hand back the pen
    * Don't take over when someone is stuck
    * Give guidance and hand back the decision

## Becoming a Multiplier

Use the right principles and tools and attain maximum results with just the right amount of effort

### The Accelerators

1. Work the Extremes
    * Assess your leadership practices
|Multiplier|Talent Magnet|Liberator|Challenger|Debate Maker|Investor|
|-|-|-|-|-|-|
|Towering Strength|-|-|-|-|-|
|Competency|-|-|-|-|-|
|Vulnerability|-|-|-|-|-|
|Diminisher|Empire Builder|Tyrant|Know-it-all|Decision Maker|Micromanager|
    * Focus development on the two extremes
        1. Bring up your lowest low
        2. Take your highest high to the next level
2. Start with the Assumptions
    * Adopt assumptions of a multiplier and allow the behaviour and practices to naturally follow
        * If I can find someone's genius, I can put them to work
        * People's best thinking must be given, not taken
        * People get smarter by being challenged
        * With enough minds, we can figure it out
        * People are smart and will figure things out
3. Take a 30-Day Multiplier Challenge
    * Pick one practice within one discipline and work it for thirty days

### Sustaining Momentum

1. Build it layer by layer
    * Build like *Bolero*
2. Stay with it for a year
    * Adopt an annual question
        * ie. *What would cause other people to become smarted and more capable around me?*
        * *How can I multiply the intelligence of others?*
3. Build a community

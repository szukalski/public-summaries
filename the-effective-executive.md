# The Effective Executive

* Know where my time goes
* Focus on results
* Build on strengths
* Focus on a few priorities
* Make effective decisions

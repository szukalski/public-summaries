# Twelve Intentional Activities for Increasing Life Satisfaction

1. Savouring life
2. Nurturing social relationships
3. Expressing gratitude
4. Committing to goals
5. Creating coping strategies
6. Practicing acts of kindness
7. Engaging in flow experiences
8. Cultivating optimism
9. Practicing spirituality
10. Taking care of your mind and body
11. Learning to forgive
12. Avoiding overthinking

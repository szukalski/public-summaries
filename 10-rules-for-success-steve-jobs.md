# 10 Rules for Success: Steve Jobs

1. Don’t Live a Limited Life
    * Everything around us has been created by someone like us.
    * There is no limit to what we can achieve.
2. Have Passion
3. Design for Yourself
4. Don’t Sell Crap
    * Only focus on the good stuff.
5. Build a Great Team
    * The greatest people are self-managing.
    * People need a common vision.
    * The best manager is a great individual contributor who becomes a manager becomes nobody else can do it better.
6. Don’t Do It for the Money
7. Be Proud of your Products
8. Build Around Customers
    * Customer experience first, then technology.
9. Marketing is about Values
    * What are your values and core values? These should not change.
10. Stay Hungry, Stay Foolish

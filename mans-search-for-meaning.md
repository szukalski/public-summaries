# Man's Search for Meaning

## Meaning of Life

* Man is questioned by life; and he can only answer to life by answering for his own life; to life he can only respond by being responsible.
* Responsibleness is the very essence of human existence.

## The Essence of Existence

* Confront the finiteness of life, and the finality of what we make out of life and ourselves.
* The meaning of life always changes, but never ceases to be.
* We can discover our meaning in three ways:
    * Purpose &rightarrow; By creating work or doing a deed.
    * Love &rarr; By experiencing something or encountering someone.
    * Suffering &rarr; By the attitude we take towards unavoidable suffering.

# 15 Ways To Stand Out on Your IT Team

Ginny Hamilton

1. Keep your skills fresh
2. Speak human
3. Learn the business
4. Collaborate with the business
5. Become a storyteller
6. Focus on the customer
7. Say "Thank You"
8. Be accountable
9. Be open-minded
10. Be a team player
11. Keep your ego in check
12. Proactively solve problems
13. Be willing to take risks
14. Don't just ideate: Deliver
15. Always be curious

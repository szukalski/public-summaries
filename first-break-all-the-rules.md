# First, Break All the Rules

## Q12

* Base Camp: What do I get?
    1. I know what is expected of me at work
    2. I have the materials and equipment I need to do my job
* Camp 1: What do I give?
    3. At work, I have the opportunity to do what I do best every day
    4. In the last seven days, I have received recognition or praise for doing good work
    5. My supervisor, or someone at work, seems to care about me as a person
    6. There is someone at work who encourages my development
* Camp 2: Do I belong here?
    7. At work, my opinions seem to count
    8. The mission or purpose of my company makes me feel my job is important
    9. My associates or fellow employees are committed to doing quality work
    10. I have a best friend at work
* Camp 3: How can we all grow?
    11. In the last six months, someone at work has talked to me about my progress
    12. This last year, I have had opportunities at work to learn and grow
* The Summit

## Wisdom of Great Managers

* What Great Managers Know
    * People don't change that much
    * Don't waste time trying to put in what was left out
    * Try to draw out what was left in
    * That is hard enough

* What Great Managers Do
    * Select a person
    * Set expectations
    * Motivate the person
    * Develop the person

* Great Managers look inward, Great Leaders look outward

## The Four Keys

* Great Managers act as catalysts
    * Select for talent
    * Define the right outcomes
    * Focus on strengths
    * Find the right fit

### 1: Select for Talent

* Talent
    * Innate and cannot be learned
    * Situation agnostic
* Skills
    * How-tos of a role
    * Can be transferred and learned
* Knowledge
    * Factual knowledge: Things you know
    * Experiential knowledge: Things you have picked up along the way

#### Three Kinds of Talent

* Striving
    * Explain the why of the person &rarr; what are the motivations, desires, and drive?
* Thinking
    * Explain the how of the person &rarr; how do they approach things?
* Relating
    * Explain the who of the person &rarr; who do they work with, who do they trust?

### 2: Define the Right Outcomes

#### Four Levels of Customer Expectation

1. Accuracy
    * I get what I ordered
2. Availability
    * I can use it when I need to
3. Partnership
    * I want to be listened to
4. Advice
    * I want to have a trusted advisor

### 3: Focus on Strengths

* Expecting non-talents to turn into talents sets up for failure and to blame the individual for this failure

#### Losers, Keepers, and Movers

* Talk with each individual about strengths, weaknesses, goals, and dreams
* Identify talents by watching their behaviour over time
* Separate people into:
    * Losers &rarr; leave the team
    * Keepers &rarr; stay in the team
    * Movers &rarr; should be moved to a different role

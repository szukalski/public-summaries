# 22 Lessons from Warren Buffet

## Risk

* Risk comes from not knowing what you’re doing.
* Should you find yourself in a chronically leaking boat, energy devoted to changing vessels is likely to be more productive than energy devoted to patching leaks.
* It is not necessary to do extraordinary things to get extraordinary results.
* After all, you only find out who is swimming naked when the tide goes out.

## Value

* Price is what you pay. Value is what you get.
* It’s far better to buy a wonderful company at a fair price than a fair company at a wonderful price.
* The investor of today does not profit from yesterday’s growth.
* Whether we’re talking about socks or stocks, I like buying quality merchandise when it is marked down.

## People

* It’s better to hang out with people better than you. Pick out associates whose behavior is better than yours and you’ll drift in that direction.
* It takes 20 years to build a reputation and five minutes to ruin it. If you think about that, you’ll do things differently.
* There seems to be some perverse human characteristic that likes to make easy things difficult.
* Of the billionaires I have known, money just brings out the basic traits in them. If they were jerks before they had money, they are simply jerks with a billion dollars.
* If you get to my age in life and nobody thinks well of you, I don’t care how big your bank account is, your life is a disaster.

## Business

* I don’t look to jump over 7-foot bars: I look around for 1-foot bars that I can step over.
* In the business world, the rearview mirror is always clearer than the windshield.
* You only have to do very few things right in your life so long as you don’t do too many things wrong.

## Investing

* Rule No.1: Never lose money. Rule No.2: Never forget rule No.1.
* We simply attempt to be fearful when others are greedy and to be greedy only when others are fearful.
* Our favorite holding period is forever.
* Never invest in a business you cannot understand.
* Someone’s sitting in the shade today because someone planted a tree a long time ago.
* Wide diversification is only required when investors do not understand what they are doing.

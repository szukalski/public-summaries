# Winning Minds

Winning Minds  
Simon Lancaster  
ISBN-10: 1137465921  
ISBN-13: 978-1137465924  

## Summary: Winning Minds

The Language of Leadership is based on progressively winning over the instinctive, emotional, and logical mind in that order.  

## The Components of the Mind

1. The Instinctive Mind
    * Draws us to rewards, keeps us away from danger
    * Keeps body functioning
    * **Leaders offer the promise of safety and reward.**
2. The Emotional Mind
    * Controls release of the emotional drugs which fuel our behaviours
    * **Leaders meet emotional needs in return for your support**
3. The Logical Mind
    * Looks for patterns, working largely on a rule-of-thumb basis
    * **The appearance of logic is enough**

## Drugs of the Mind

* Oxytocin
    * Love/connection/cuddly
* Seratonin
    * Pride/esteem/confidence
* Cortisol
    * Stress/fear
* Dopamine
    * Reward

## Winning the Instinctive Mind

**Avoid Danger and Find Rewards**

* Metaphors
    * Substitute one thing for another (x = y)
* The Look of Leadership
    * Strong, sincere, and sexy
* Inner Purpose
    * Communicate a vision &rarr; potential for reward
    * Show progress against the vision &rarr; reward &rarr; dopamine
* Empathy and the Power of Nice
    * Make a personal connection and listen &rarr; oxytocin and seratonin
* Smiles and Humour
    * Smiles and laughter make people feel great &rarr; oxytocin
* Breathing
    * Influence and lead the mood of those around &rarr; imply danger or safety
* Style
    * Have clear visions and present them in clear language
    * Connect to the audience in a similar style

## Winning the Emotional Mind

**Provide the emotional drugs that everyone seeks**

* Stories and Emotion
    * Storyteller is a natural authority
    * Activates mirror neurons
    * 3 parts to a story:
        * Great characters produce oxytocin
        * Great dilemmas produce cortisol
        * Great resolution produces dopamine
* Personal Stories
    * Provide connection in a short period of time
* Creating Culture
    * Cultures are collections of stories
    * Sharing stories shapes culture
* Harnessing History
    * History is filled with deeply emotional and evocative stories
* Values
    * Higher purpose and values deliver high performance
* Word Selection
    * Positive associations boost performance
    * Emotional words are simple
    * I/Me/You/Your/Us/Our, Great, Love &rarr; Powerful
* Flattery
    * People love to be celebrated, even when it is not wholly sincere &rarr; seratonin
* Repetition
    * Communicates emotion
    * More likely to believe a statement if heard before, even if not true
* Exaggeration
    * When we are emotional, our perspective becomes distorted
    * Exaggeration excites &rarr; endorphins

## Winning the Logical Mind

**Enough to give the appearance of truth. We seek patterns to determine truth**

* Rule of Three
    * Creates balance (3) between two extremes (1 and 2)
* Balance
    * If it *sounds* balanced then we assume it is balanced
    * Sentences must be balanced (x... y...)
    * Alliteration creates balance
* Rhyme
    * People are more likely to believe *anything* is true if it rhymes
* Perspective
    * Most people do not have a fixed view and are open to changing perspectives
    * Get the mood right in the beginning, even with vague statements with no real purpose
    * Start from a strongly believed universal agreement &rarr; people do not like to be inconsistent and want to keep agreeing
* Numbers
    * People do not understand numbers
    * Use numbers only to create powerful impressions and images
    * Put numbers up against something which makes them look incredibly large or incredibly small
* Brevity
    * Keep it short

## Cicero's Speech Structure

1. Exposition
    * What is the theme?
2. Narrative History
    * What is the history to date?
3. Division
    * What is the question?
4. Evidence in Support
5. Refutation
    * Arguments against those who oppose our view
6. Summary
    * Our answer to the question

## The Secret Science of the Language of Leadership

* Leaders arouse huge strength of feeling
    * The touch us deeply and emotionally
* Great leadership is about great communication
    * Communication now is harder than ever with the advent of mobile phones

### Rhetoric

* Aristotle said great communication requires three things:
    1. **Ethos** - Credibility: Can I trust you?
    2. **Pathos** - Emotion: Do I care about what you are saying?
    3. **Logos** - Logic, or the appearance of logic: Are you right, or do you sound right?
* Great leaders need the answer to **all 3** to be yes
    * Most communication focuses on logic without emotion or credibility

### Mirror Neurons

* When people see someone acting with purpose, they mirror in their minds what the other person is doing
    * Their brains respond as if they are performing the task themselves

### Introduction to the Brain

* Instinctive Brain
    * Sits at base of brain
        * Unconscious or reptilian brain
    * 95% of brain activity
    * Operating mandate is to ensure our survival
        1. Keeps body functioning
        2. Provides instinctive impulses to draw us to rewards, and away from danger
    * One flaw: has not changed in the last million years whilst the environment has
    * **Leaders speak to the instinctive brain's needs. The offer the promise of safety and rewards**
* Emotional Brain
    * 20x as powerful as the logical brain
    * When we feel emotional, powerful drugs are released which flood our mind and reduce our capacity for logical thought
        * Oxytocin (love/connection/cuddly)
        * Seratonin (pride/esteem/confidence)
        * Cortisol (stress/fear)
    * We spend much of our time seeking emotional fulfilment
        * **Great leaders meet people's emotional needs. In return, they are given support**
* Logical Brain
    * 85% of brain mass and the newest
    * Not as clever as it seems
        * Looks for patterns, working largely on a rule-of-thumb basis
        * The *appearance* of logic can suffice to prove a point

### Winning the Instinctive Mind

* Two prime needs: Avoid danger and find rewards
    * Leader must be seen as a friend, not a foe
* How we feel affects how others feel
    * No enthusiasm == no chance
* We are sensitive to one other's breathing patterns
    * Communicates 2 things:
        * Is this person healthy enough to be our leader?
        * Is the environment we are in safe?
    * Short, sharp sentences suggest fear
    * Deep, calm sentences convey confidence
* Genuine, heartfelt smiles are quickest wins
* Metaphor speaks to the instinctive mind
    * Often proves decisive in winning or losing an argument
    * Find ones that help, and avoid ones that hinder

### Winning the Emotional Mind

* The emotional brain is like a big pharmacy we are desperate to break into
    * Repetition
    * Praise causes serotonin to be released (calm/confidence)
        * Promotes high performance
    * A good story
        * Connecting to the protagonist &rarr; oxytocin (connection)
        * Setting out the critical dilemma of the story &rarr; cortisol (stress)
        * Resolving the dilemma &rarr; dopamine (reward)
* Seek to change behaviour by releasing oxytocin and cortisol

### Winning the Logical Mind

* When we hear people speak, the words go to two different parts of the brain:
    * One part analyses the meaning of what is said
    * One part analyses the music
* Not just the reasoning, but the rhythm
    * Tricolon - Rule of Three
        * Creates the illusion of completeness, certainty, and conviction
    * Rhyme
        * People more likely to believe something which rhymes
        * Considered signifiers of truth

### The Language of Leadership

* APET Model
    * Acronym:
        * **A**ctivating agent
        * **P**attern-matching
        * **E*motion
        * **T**houghts

**Stimulus** &rarr; **Instinctive** Looks for patterns &rarr; **Emotional** Releases hormones &rarr; **Logical** Begins cognitive thought

* Language of Leadership based on great leaders progressively winning over the instinctive, emotional, and logical mind - in that order
    * Must win over **all three**
    * Critical starting point is the instinctive brain

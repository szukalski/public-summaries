# Parenting: Who is it really for?

Derek Sivers
https://sivers.org/pa

Parenting things you do for your kids are also for you

## Long Attention Span

* Cultivate a long attention span
* Spend hours immersed in new games
* If your mind wanders then let it go and return to the present focus

## Enter Their World

* With your child, stop everything else
    * Phone off. Computer off
* Try to enter their world
    * See through their eyes
    * Put yourself into their mind
    * See their emotions from their side
* They are the most important. More than work or phone

## Broad Inputs

* Expose them to a wide range of inputs into their senses
* Touch and smell everything you can
* Broad music styles
* Lots of books

## These things are for yourself as much as them

* Cultivating your own long attention span
* Letting go of your world to go into theirs
* Broadening your inputs by broadening theirs

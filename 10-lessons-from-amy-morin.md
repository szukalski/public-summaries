# 10 Lessons from Amy Morin

1. School isn't enough. You need to do
2. A few bad habits can derail all good habits
3. Avoid toxic people
4. Don't wait until you feel broken
5. All is not lost
6. Don't avoid
7. You can always do something
8. Nobody knows anything
9. The law of emotional relativity
10. Live life like it's everyone else's last day
